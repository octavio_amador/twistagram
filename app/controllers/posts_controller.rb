class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  protect_from_forgery :except => [:index]

  # GET /posts
  # GET /posts.json
  def index
    if user_signed_in?
      @posts = current_user.posts
    else
      @posts = Post.all
    end
  end

  # GET /posts/1
  # GET /posts/1.json

  def index
          @posts = Post.all
            respond_to do |format|
                format.json do
                    render :json => @posts, :callback => params[:callback]
                end
                format.html
            end
     # if admin_signed_in?
     # else
         # @asesorias = current_user.asesorias
      #end
  end

  def show
  end

  # GET /posts/new
  def new
      @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        flash[:success] = "Post was successfully created."
        format.html { redirect_to @post }
        format.json { render :show, status: :created, location: @post }
      else
        flash[:error] = "There was a problem while saving the post"
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        flash[:success] = "Post was successfully updated."
        format.html { redirect_to @post }
        format.json { render :show, status: :ok, location: @post }
      else
        flash[:error] = "There was a problem while updating the post"
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end
    #holo
    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:image, :title, :body)
    end
end
