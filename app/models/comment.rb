class Comment < ActiveRecord::Base
	belongs_to :post
	validates_length_of :body, :in => 20..400, :message => "la longitud debe ser entre 20 y 400 caracteres"
end
